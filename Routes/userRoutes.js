const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const userController = require("../Controllers/userController.js")


// [Routes]

//this is reponsible for the registration of the user
router.post("/register", userController.userRegistration);

//Route fo user authentication
router.post("/login",userController.userAuthentication);

//Route to get user's profile
router.get("/details",auth.verify, userController.getProfile);

//route for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);






module.exports = router;