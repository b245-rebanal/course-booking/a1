

const express = require("express");
const router = express.Router();
const courseController = require("../Controllers/courseController.js");
const auth = require("../auth.js");

//Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

//Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses);

//Route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses);


//Route for retrieving all inactive courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses);

//Route for retrieving detail/s of specific course
router.get("/:courseId", courseController.courseDetails);

//Route for archiving course
router.put("/:courseId", auth.verify, courseController.archiveCourse);

router.put("/update/:courseId", auth.verify, courseController.updateCourse);


module.exports = router;