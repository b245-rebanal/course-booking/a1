//

const express = require("express");
const mongoose = require("mongoose");
// by default our backend's CORS setting will prevent any application outside our Express JS app to process the request.Using the cors package, it will will allow us to manipulate thsi and control what applications may use our app

//Allows our backend application to be available to our frontend application
//Allows us to control the app's Cross Origin Resourse Sharing
const cors = require("cors");

const userRoutes = require("./Routes/userRoutes.js");
const courseRoutes = require("./Routes/courseRoutes.js");

const port = 3001;
const app = express();
	
	// [MongoDB Connection]
	mongoose.set('strictQuery', true);
	mongoose.connect("mongodb+srv://admin:admin@batch245-rebanal.hxprcml.mongodb.net/batch245_course_api_rebanal?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})


	let db = mongoose.connection;

	//For error handling
	db.on("error", console.error.bind(console, "Connection Error!"));

	//For validation of the connection
	db.once("open", () => {console.log('We are connected to the cloud!')});




// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());
//routing
app.use("/user", userRoutes);
app.use("/course", courseRoutes);



app.listen(port, ()=> console.log(`Server is running at Port ${port}`))

