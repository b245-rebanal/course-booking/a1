const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Controllers


//This controller will create or register a user on our db/database
module.exports.userRegistration = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already taken!")
			}else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					mobileNo: input.mobileNo
				})


				//save to database
				newUser.save()
				.then(save => {
					return response.send("You are now registered to our website!")
				})
				.catch(error => {
					return response.send(error)
				})
			}
		})
		.catch(error => {
			return response.send(error)
		})
}
//User Authentication
module.exports.userAuthentication = (request,response)=>{
	let input = request.body;
	//Possible scenarios in logging in
			//1. email is not yet registered.
			//2. email is registered but the password is wrong

	User.findOne({email:input.email})
	.then(result =>{
		if(result===null){
			return response.send("Email is not yet registered. Register first before logging in!")
		}else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)})
			}else{
				// we have to verify if the passwor is correct
				// The "compareSync" method is used to compare a non encrypted password to the encrypted password.
				//it returns boolean value, if match true value will return otherwise false.

				return response.send("Pasword is incorrect!")
			}
		}
	})
	.catch(error =>{
		return response.send(error);
	} )
}
//activity
module.exports.getProfile = (request, response) => {

		//const input = request.body;
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
		return User.findById(userData._id)
		.then(result => {
				result.password = "Confidential"
				return response.send(result);
			
		})
		.catch(error => {
			return response.send(error)
		})
}


module.exports.enrollCourse = async (request, response) => {
	// First we have to get the userId and the courseId

	//decode the token to extract/unpack the payload
	const userData = auth.decode(request.headers.authorization);

	//get the courseId by targetting the params in the url
	const courseId = request.params.courseId;

	//2 things that we need to do in this controller
		//First, to push the courseId in the enrollments property of the user
		//second, to push the userId in the enrolles property of the course.

	let isUserUpdated = await User.findById(userData._id)
	.then(result => {
		if(result === null){
			return false
		}/*else{
			result.enrollments.push({courseId: courseId});

			return result.save()
			.then(save => true)
			.catch(error => false)
		}*/
//S41 ACTIVITY
		else{
			if(userData.isAdmin){
				return response.send("Only Non-admin can enroll!");
			}else{
				result.enrollments.push({courseId: courseId});

				return result.save()
				.then(save => true)
				.catch(error => false)
			}
			
		}
	})

	let isCourseUpdated = await Course.findById(courseId).then(result => {
		console.log(result);
		if(result === null){
			return false;
		}else{
			result.enrollees.push({userId: userData._id});

			return result.save()
			.then(save => true)
			.catch(error => false);
		}
	})

	console.log(isCourseUpdated);
	console.log(isUserUpdated);

	if(isCourseUpdated && isUserUpdated){
		return response.send("The course is now enrolled!");
	}else{
		return response.send("There was an error during the enrollment. Please try again!");
	}

};