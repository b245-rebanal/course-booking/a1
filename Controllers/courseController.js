


const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");

/*
Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) =>{
	let input = request.body;
	let userData = auth.decode(request.headers.authorization);
	let newCourse = new Course({
		name : input.name,
		description: input.description,
		price: input.price
	});
	// saves the created object to our database
	if(!userData.isAdmin){
		return response.send("You are not an admin!")
	}

	else{

	return newCourse.save()
	// course succesfully created
	.then(course =>{
		console.log(course);
		response.send(course);
	})
	// course creation failed
	.catch (error=>{
		console.log(error);
		response.send(false);
	})
	}
}

module.exports.allCourses = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send("You are not an admin!")
	}

	else{
		Course.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}

}

module.exports.allActiveCourses = (request, response) => {

	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}


module.exports.allInactiveCourses = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send("You don't have acces to this route!");
	}else{
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}

//This controller will get the details of specific course
module.exports.courseDetails = (request, response) => {
	
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}


// S40 ACTIVITY
module.exports.archiveCourse = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send("You don't have acces to this route!");
	}else{
		Course.findById(courseId)
		.then(result => {
			if(result === null){
				return response.send("No existing course ID!")
			}else{
				let courseArchive = {
					isActive: input.isActive
				}


				Course.findByIdAndUpdate(courseId, courseArchive, {new: true})
				.then(result => {
					return response.send(true)
				})
				.catch(error => {
					return response.send(error)
				})
			}
		})
		.catch(error => response.send(error));
	}

}


//This controller is for updating specific course
/*
	Business logic:
		1. We are going to edit/update the course that is stored in the params 
*/

module.exports.updateCourse = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body;


	if(!userData.isAdmin){
		return response.send("You don't have acces in this page!")
	}else{
	
	await Course.findOne({_id: courseId})
		.then(result =>{
			if(result === null){
				return response.send("courseId is invalid, please try again!")
			}else{
				let updatedCourse= {
					name: input.name,
					description: input.description,
					price: input.price
				}
				
				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					console.log(result);
					return response.send(result)})
				.catch(error => response.send(error));
			}
		})
		
	}
}